
const chatItem = document.querySelector('.chat__item');

const textAteaInput = document.querySelector('#masseng__textarea');

const btnSend = document.querySelector('.btn__send');

const userLogin = document.querySelector('.login__name');

let author = 'Admin';

let date;

let nowTime;

function messengNew (newMassenge) {
    return `
        <div class="massege__item">

            <div class="header__massege">
            
                <p class="author__text">${newMassenge.author}</p>
            
                <div class="block__information">       
                    <a href="https://ru.wikipedia.org/wiki/%D0%92%D1%81%D0%B5%D0%BC%D0%B8%D1%80%D0%BD%D0%BE%D0%B5_%D0%BA%D0%BE%D0%BE%D1%80%D0%B4%D0%B8%D0%BD%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%BD%D0%BE%D0%B5_%D0%B2%D1%80%D0%B5%D0%BC%D1%8F" target="_blank">
                        <p class="date__text">${newMassenge.datetime.toString().slice(11, 19) + ' UTF'}</p>
                    </a>
                </div>
            
            </div>

            <div class="body__massege">
                <img src="icon.png" alt="" srcset="">
                <p class="joke__text">${newMassenge.message}</p>
            </div>

        </div>
    `;
}

function massengeViewInChat (objMassenge) {
    let lastMassengTime = [objMassenge.length - 1];
    
    nowTime = objMassenge[lastMassengTime].datetime;

    for (let key in objMassenge){
    
        chatItem.innerHTML += messengNew(objMassenge[key]);
    
    }

}


const enterLogin = (child, input) => (event) => {

    if (event.key === 'Enter') {

        if (input.value.trim().length > 3) {

            child.innerHTML = input.value.trim();
            input.style.display = 'none';
            child.style.display = 'block';

            author = input.value.trim();

        } else {
            input.style.outline = 'red 1px solid';

            alert(`Пожалуйста ввкедите больше чем 3 символа`);

        }

    }

};

fetch(`http://146.185.154.90:8000/messages`)
    .then(data => {return data.json();})

    .then(data => {
        nowTime = data[data.length - 2].datetime;
    }).then(() => {

        setInterval(() => {

            fetch(`http://146.185.154.90:8000/messages?datetime=${nowTime}`)
                .then(data => {return data.json();})
                
                .then(data => {

                data.length >= 1 ? massengeViewInChat(data) : [];
                });

        }, 2000);

    });

btnSend.addEventListener('click', (event) => {
    event.preventDefault();

    if (textAteaInput.value.trim() != ''){

        const data = {
            'message': textAteaInput.value.trim(),
            author: author
        };

        let array = [];

        for(let key in data){
            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);
            array.push(encodedKey + "=" + encodedValue);
        }

        array = array.join("&");

        fetch('http://146.185.154.90:8000/messages', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: array
        });

    }else {
        alert('Введите что нибудь');
    }

    textAteaInput.value = '';

});

userLogin.addEventListener('dblclick', (event) => {
    
    newUserLogin(userLogin);

});

function newUserLogin (child) {

    const input = document.createElement('input');

    child.parentElement.append(input);

    child.style.display = 'none';

    input.addEventListener('keydown', enterLogin(child, input));
}
